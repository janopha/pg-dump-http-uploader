FROM alpine

VOLUME /pg_dump

RUN apk update && apk add curl gnupg postgresql-client tzdata

COPY pg_dump.sh .
RUN chmod +x ./pg_dump.sh

ENTRYPOINT ["sh", "/pg_dump.sh"]