#!/bin/bash

set -e

ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime && echo $TIMEZONE > /etc/timezone

echo "Importing gpg keys..."

echo $GPG_PUBLIC_KEY | base64 -d | gpg --import

echo $GPG_PRIVATE_KEY | base64 -d | gpg --allow-secret-key-import --import

GPG_RECIPIENT_PUBLIC_KEY_ID=$(echo $GPG_RECIPIENT_PUBLIC_KEY | base64 -d | gpg --import 2>&1 | awk -F '[ :]' 'NR==1 {print $4; exit}')

echo -e "\npg_dump started: $(date)"

PG_DUMP_DATE=$(date +%Y%m%d_%H%M%S)
PG_DUMP_FILE="$PGDATABASE-pg_dump-$PG_DUMP_DATE.sql"

pg_dump -f /tmp/$PG_DUMP_FILE --inserts

echo "pg_dump finished: $(date) and saved as: $PG_DUMP_FILE"

gpg -o /pg_dump/$PG_DUMP_FILE.gpg -e -s -a -r "$GPG_RECIPIENT_PUBLIC_KEY_ID" --trust-model always /tmp/$PG_DUMP_FILE

echo -e "\nEncrypted pg_dump for recipient $GPG_RECIPIENT_PUBLIC_KEY_ID."

rm /tmp/$PG_DUMP_FILE

echo -e "\nUploading $PG_DUMP_FILE file..."

curl --user $UPLOAD_USERNAME:$UPLOAD_PASSWORD --upload-file /pg_dump/$PG_DUMP_FILE.gpg $UPLOAD_URL/$PG_DUMP_FILE.gpg

echo -e "\nUpload successful!"
