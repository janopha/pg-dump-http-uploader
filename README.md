# pg_dump HTTP Uploader

This Docker image makes a dump of a Postgres database. This dump is encrypted with gpg and uploaded via curl.

## ENV
* TIMEZONE
* PGHOST
* PGPORT
* PGDATABASE
* PGUSER
* PGPASSWORD
* UPLOAD_URL
* UPLOAD_USERNAME
* UPLOAD_PASSWORD
* GPG_PUBLIC_KEY (base64 encoded)
* GPG_PRIVATE_KEY (base64 encoded)
* GPG_RECIPIENT_PUBLIC_KEY (base64 encoded)
